## Pre-requisites

1. Access to EKS 
2. Client tooling, on `macOS`:
```
brew install awscli kubectl aws-vault derailed/k9s/k9s kubectx kubens helm
```

## Install Spring Boot Admin Server Helm chart

To discover Spring Boot packaged Java services, it uses [Discovery Client for Kubernetes](https://cloud.spring.io/spring-cloud-kubernetes/reference/html/#discoveryclient-for-kubernetes)
and more exactly `spring-cloud-kubernetes-fabric8-discovery`. See as well
[Service Account docs](https://docs.spring.io/spring-cloud-kubernetes/docs/current/reference/html/#service-account).

Install the Helm chart inside any convenient namespace if you want discovery of Spring Boot applications from multiple namespaces
(which is also the default, see configuration value `.Values.multi.namespaced`), otherwise same namespace where your Spring Boot applications are deployed
and update `.Values.multi.namespaced` to `false` so that only `Role` and `RoleBinding` are created instead of `ClusterRole` and `ClusterRoleBinding`.

You might run more than just Spring Boot applications in your Kubernetes namespace, so we need to define a filter for which services to scrape.
This can be done with the property `spring.cloud.kubernetes.discovery.service-labels`.
Adapt Helm chart property placeholder `.Values.app.serviceLabelNameValue` to your use-case or set the value command-line.
Make sure Kubernetes services for Spring Boot applications you desire to scrape have that label name with the given value.

You can customise the UI title via updating `.Values.ui.title`

Once installed, Spring Boot Admin UI URL will be accessible via port forwarding, thus cluster access is required.
Default context is `/admin`, you can update value via setting `.Values.app.context` at Helm chart installation.

### See all generated YAML for deployment:

```
$ helm template --set namespace=[your_k8s_namespace] \
  --set app.serviceLabelNameValue=[your_svc_label_name_and_value] \
  ./spring-boot-admin --debug
```

### Create the k8s objects from scratch:

```
$ helm install --set namespace=[your_k8s_namespace] \
  --set app.serviceLabelNameValue=[your_svc_label_name_and_value] \
  spring-boot-admin ./spring-boot-admin --debug
```

### Update the k8s objects:

```
$ helm upgrade --install --set namespace=[your_k8s_namespace] \
  --set app.serviceLabelNameValue=[your_svc_label_name_and_value] \
  spring-boot-admin ./spring-boot-admin --debug
```

### Uninstall Spring Boot Admin Server Helm chart:

```
$ helm uninstall spring-boot-admin
```